package eventoscientificos;

import eventoscientificos.model.*;
import java.io.*;
import java.util.List;
import javax.swing.*;

public class Descodificador {

    public static void ler(List<Evento> listaEventos, List<Utilizador> listaUtilizadores) throws FileNotFoundException {
        try {
            File ficheiro = new File("Teste.txt");
            BufferedReader ler = new BufferedReader(new FileReader(ficheiro));

            // variaveis utilizadas para inicializar Utilizador
            String nome, email, username, password;

            //variaveis utilizadas para inicializar Evento
            String titulo, descricao, dataI, dataF, local;

            //variaveis utilizadas para inicializar Sessao Tematica
            String codigo, descricaoS;

            String linha;

            while ((linha = ler.readLine()) != null) {
                if (linha.substring(0).equalsIgnoreCase("*")) {
                    nome = linha.substring(linha.indexOf("*") + 1, linha.indexOf(";"));
                    email = linha.substring(linha.indexOf(";") + 1, linha.indexOf(";", linha.lastIndexOf(nome) + 2));
                    password = linha.substring(linha.indexOf(";", linha.lastIndexOf(nome) + 2) + 1, linha.indexOf(";", linha.lastIndexOf(email) + 2));
                    username = linha.substring(linha.indexOf(";", linha.lastIndexOf(email) + 2) + 1, linha.indexOf(";", linha.lastIndexOf(password) + 2));

                    listaUtilizadores.add(new Utilizador(nome, email, username, password));

                } else if (linha.substring(0).equalsIgnoreCase("-")) {
                    titulo = linha.substring(linha.indexOf("+") + 1, linha.indexOf(";"));
                    descricao = linha.substring(linha.indexOf(";") + 1, linha.indexOf(";", linha.lastIndexOf(titulo) + 2));
                    dataI = linha.substring(linha.indexOf(";", linha.lastIndexOf(titulo) + 2) + 1, linha.indexOf(";", linha.lastIndexOf(descricao) + 2));
                    dataF = linha.substring(linha.indexOf(";", linha.lastIndexOf(descricao) + 2) + 1, linha.indexOf(";", linha.lastIndexOf(dataI) + 2));
                    local = linha.substring(linha.indexOf(";", linha.lastIndexOf(dataI) + 2) + 1, linha.indexOf(";", linha.lastIndexOf(dataF) + 2));

                    listaEventos.add(new Evento(titulo, descricao, dataI, dataF, local));
                } else if (linha.substring(0).equalsIgnoreCase("+")) {
                    codigo = linha.substring(linha.indexOf("+") + 1, linha.indexOf(";"));
                    descricaoS = linha.substring(linha.indexOf(";") + 1, linha.indexOf(";", linha.lastIndexOf(codigo) + 2));

                    listaEventos.get(listaEventos.size()).addSessaoTematica(codigo, descricaoS);
                }
            }
        } catch (IOException e) {
            JOptionPane.showInputDialog(null, "Não é possível ler o ficheiro");
        }
    }
}
