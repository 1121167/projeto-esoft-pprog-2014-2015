package eventoscientificos.ui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;

public class SubmeterArtigoUI extends JFrame {

    private JList lstAutores;
    private JButton btnSubmeter, btnCancelar;
    private JComboBox cbEvento, cbAutorC;
    private JFileChooser fcPDF;
    private JTextField txtTitulo;
    private JTextArea txtResumo;

    public SubmeterArtigoUI() {
        super("Eventos Cientificos - Submeter Artigo");

        setLayout(new BorderLayout());

        Componentes();

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void Componentes() {
        JPanel p1 = criarPainelNorte();
        JPanel p2 = criarPainelOeste();
        JPanel p3 = criarPainelEste();
        JPanel p4 = criarPainelSul();

        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.WEST);
        add(p3, BorderLayout.EAST);
        add(p4, BorderLayout.SOUTH);
    }

    private JPanel criarPainelNorte() {
        JPanel p = new JPanel(new FlowLayout());
        p.add(criarPainelEvento());
        
        return p;
    }

    private JPanel criarPainelOeste() {
        
        JPanel p = new JPanel(new BorderLayout());
        
        p.add(criarPainelTitulo(), BorderLayout.NORTH);
        p.add(criarPainelResumo(), BorderLayout.CENTER);
        p.add(criarPainelAutorCorrespondente(), BorderLayout.SOUTH);
        
        return p;
    }

    private JPanel criarPainelEste() {
        JPanel p = new JPanel(new FlowLayout());
        p.add(criarListaAutores());
        
        return p;
    }

    private JPanel criarPainelSul() {
        JPanel p = new JPanel(new GridLayout(2, 1));
        p.add(criarPainelFicheiro());
        p.add(criarPainelBotoes());

        return p;
    }
    
    private JPanel criarPainelEvento(){
        JLabel lbl = new JLabel("Evento:", JLabel.RIGHT);

        cbEvento = new JComboBox();
        cbEvento.setSelectedItem(null);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.CENTER));
        p.add(lbl);
        p.add(cbEvento);

        return p;
    }
    
    private JPanel criarPainelTitulo(){
        JLabel lbl = new JLabel("Título:", JLabel.RIGHT);

        txtTitulo = new JTextField(20);
        txtTitulo.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(lbl);
        p.add(txtTitulo);

        return p;
    }
    
    private JPanel criarPainelResumo(){
        JLabel lbl = new JLabel("Resumo:", JLabel.LEFT);
        
        txtResumo = new JTextArea(9, 19);
        txtResumo.requestFocus();
        
        JPanel p = new JPanel(new BorderLayout());
        p.add(lbl, BorderLayout.NORTH);
        p.add(txtResumo, BorderLayout.SOUTH);
        
        return p;
    }
    
    private JPanel criarPainelAutorCorrespondente(){
        JLabel lbl = new JLabel("Autor Correspondente:", JLabel.RIGHT);
        
        cbAutorC = new JComboBox();
        cbAutorC.setSelectedItem(null);
        
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(lbl);
        p.add(cbAutorC);
        
        return p;
    }
    
    private JPanel criarListaAutores(){
        JLabel lblTitulo = new JLabel("Lista de Autores:", JLabel.LEFT);
        lstAutores = new JList();
        lstAutores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane sp = new JScrollPane (lstAutores);
        
        
        JPanel p = new JPanel(new BorderLayout());
        
        p.add(lblTitulo, BorderLayout.NORTH);
        p.add(sp, BorderLayout.CENTER);
        p.add(criarBotoesListaAutores(), BorderLayout.SOUTH);
        
        return p;
    }
    
    private JPanel criarBotoesListaAutores(){
        JPanel p = new JPanel(new GridLayout(2, 1,0,10));
        p.setBorder(BorderFactory.createEmptyBorder(10,0,0,0));
        
        p.add(criarBotaoNovoAutor());
        p.add(criarBotaoEliminarAutor());
        
        return p;
    }
    
    private JButton criarBotaoNovoAutor(){
        JButton btnAutor = new JButton("Novo Autor");
        btnAutor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new DialogNovoAutor(SubmeterArtigoUI.this, "Novo Autor");
            }
        });
        return btnAutor;
    }
    
    private JButton criarBotaoEliminarAutor(){
        JButton btnEliminar = new JButton("Eliminar Autor");
        return btnEliminar;
    }
    
    private JPanel criarPainelFicheiro() {
        JLabel lbl = new JLabel("Ficheiro:", JLabel.RIGHT);
        JTextField txtCaminho = new JTextField(20);
        JButton btn = new JButton("Procurar");
        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int resolt = fcPDF.showOpenDialog(SubmeterArtigoUI.this);

                if (resolt == JFileChooser.APPROVE_OPTION) {
                    File file = fcPDF.getSelectedFile();
//                    ListaTelefonica listaTelefonica
//                            = ficheiroListaTelefonica.ler(file.getPath());
//                    if (listaTelefonica == null) {
//                        JOptionPane.showMessageDialog(
//                                SubmeterArtigoUI.this,
//                                "Impossível ler o ficheiro: " + file.getPath()+ " !",
//                                "Importar",
//                                JOptionPane.ERROR_MESSAGE);
//                    } else {
//                        ModeloTabelaListaTelefonica modeloTabela
//                                = (ModeloTabelaListaTelefonica) tableListaTelefonica.getModel();
//                        int totalTelefonesAdicionados = modeloTabela.addRows(listaTelefonica);
//                        JOptionPane.showMessageDialog(
//                                SubmeterArtigoUI.this,
//                                "Ficheiro Submetido",
//                                "Importar Lista Telefónica",
//                                JOptionPane.INFORMATION_MESSAGE);
                    }
            }
        });

        JPanel p = new JPanel(new FlowLayout(FlowLayout.CENTER));
        p.add(lbl);
        p.add(txtCaminho);
        p.add(btn);

        return p;
    }

    private JPanel criarPainelBotoes() {
        btnSubmeter = criarBotaoSubmeter();
        getRootPane().setDefaultButton(btnSubmeter);

        btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        p.add(btnSubmeter);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoSubmeter() {
        JButton btn = new JButton("Submeter");
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}
