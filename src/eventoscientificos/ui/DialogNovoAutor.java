package eventoscientificos.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author Sofia
 */
public class DialogNovoAutor extends JDialog {

    private JTextField txtNome, txtAfiliacao;
    private JFrame framePai;

    DialogNovoAutor(JFrame framePai, String novo_Autor) {
        super(framePai, "Novo Autor", true);

        this.framePai = framePai;

        setLayout(new GridLayout(0, 1));
        add(criarPainelNome());
        add(criarPainelAfiliacao());
        add(criarPainelBotoes());

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        pack();
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private Component criarPainelNome() {
        JLabel lbl = new JLabel("Nome:", JLabel.RIGHT);

        txtNome = new JTextField(20);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(lbl);
        p.add(txtNome);

        return p;
    }

    private Component criarPainelAfiliacao() {
        JLabel lbl = new JLabel("Afiliacao:", JLabel.RIGHT);

        txtAfiliacao = new JTextField(25);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(lbl);
        p.add(txtAfiliacao);

        return p;
    }

    private Component criarPainelBotoes() {
        JButton btnOK = criarBotaoOK();
        getRootPane().setDefaultButton(btnOK);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        p.add(btnOK);
        p.add(btnCancelar);

        return p;
    }

    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String nome = txtNome.getText();
//                    PosicaoJogador posicao = (PosicaoJogador) (comPosicao.getSelectedItem());

//                    ModeloListaJogadores modeloListaJogadores = new ModeloListaJogadores();
//                    modeloListaJogadores.addElement(new Jogador(nome, posicao));
                    setVisible(false);

                    JOptionPane.showMessageDialog(framePai, "Autor adicionado.");

                    dispose();

                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(
                            framePai,
                            ex.getMessage(),
                            "Novo Autor",
                            JOptionPane.WARNING_MESSAGE);
                    txtNome.requestFocus();
                }
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}
