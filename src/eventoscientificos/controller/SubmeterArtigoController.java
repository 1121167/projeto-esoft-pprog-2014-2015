package eventoscientificos.controller;

import eventoscientificos.model.*;
import java.io.*;
import java.util.*;

public class SubmeterArtigoController {

    private Empresa m_empresa;
    private Submissao m_submissao;
    private Submissivel m_submissivel;
    private Artigo m_artigo;
    private Evento m_evento;
    private SessaoTematica m_sessao;

    public SubmeterArtigoController(Empresa empresa) {
        this.m_empresa = empresa;
    }

    public Empresa getEmpresa() {
        return m_empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.m_empresa = empresa;
    }

    public List<Submissivel> getListaSubmissiveis() {
        return m_empresa.getListaSubmissiveis();
    }

    public void novaSubmissaoEm(Submissao s) {
        m_submissivel.novaSubmissao();
    }

    public void setDados(String titulo, String resumo) {
        m_artigo.setTitulo(titulo);
        m_artigo.setResumo(resumo);
    }

    public Autor novoAutor(String nome, String afiliacao, String email) {
        return m_artigo.novoAutor(nome, afiliacao, email);
    }

    public void addAutor(Autor aut) {
        m_artigo.addAutor(aut);
    }

    public List<Autor> getPossiveisAutoresCorrespondentes() {
        List<Autor> lstPossiveisAutoresCorrespondentes = new ArrayList<>();
        List<Autor> lstAutoresDoArtigo = this.m_artigo.getListaAutores();
        List<Utilizador> lstUtilizadores = this.m_empresa.getListaUtilizadores();
        for (Autor autor : lstAutoresDoArtigo) {
            for (Utilizador utilizador : lstUtilizadores) {
                if ((autor.getEmail()).equalsIgnoreCase(utilizador.getEmail())) {
                    lstPossiveisAutoresCorrespondentes.add(autor);
                }
            }
        }
        return lstPossiveisAutoresCorrespondentes;
    }

    public void setCorrespondente(Autor autor) {
        m_artigo.setAutorCorrespondente(autor);
    }

    public void setFicheiro(File fx) {
        m_artigo.setFicheiro(fx);
    }

    public String getInfoResumo() {
        return m_artigo.getResumo();
    }

    public boolean registarSubmissao() {
        m_submissao.setArtigo(m_artigo);
        m_submissivel.addSubmissão(m_submissao);
        return false;
    }
}

