package eventoscientificos.model;

import java.util.*;

public class Evento implements Submissivel {

    // Variaveis Instancia
    private String m_strTitulo;
    private String m_strDescricao;
    private String m_strDataInicio;
    private String m_strDataFim;
    // Variaveis de classe
    private Local m_local;
    private List<Organizador> m_listaOrganizadores;
    private List<SessaoTematica> m_listaSessaoTematica;
    private List<Submissao> m_listaSubmissoes;
    private CP m_cp;
    private Empresa m_empresa;

    /**
     *
     * Método construtor utilizado com informação acerca de um evento.
     *
     * @param titulo , titulo do evento
     * @param descricao , descriçao do evento
     * @param dataI , data de inicio do evento
     * @param dataF , data final do evento
     * @param local , local do evento
     */
    public Evento(String titulo, String descricao, String dataI, String dataF, String local) {
        this.m_strTitulo = titulo;
        this.m_strDescricao = descricao;
        this.m_strDataInicio = dataI;
        this.m_strDataFim = dataF;
        m_local = new Local();
        m_local.setLocal(local);
        m_listaOrganizadores = new ArrayList<>();
        m_listaSubmissoes = new ArrayList<>();
    }

    /**
     * Metodo construtor sem informaçao do evento
     */
    public Evento() {
        m_local = new Local();
        m_listaOrganizadores = new ArrayList<>();
        m_listaSubmissoes = new ArrayList<>();
//        m_empresa.addEvento(this);
    }

    /**
     * Metodo para inicializar uma nova CP do evento
     *
     */
    public CP novaCP() {
        return m_cp = new CP();
    }

    /**
     * Metodo utilizado para modificar o titulo do evento
     *
     * @param strTitulo, titulo do evento
     */
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    /**
     * Metodo utilizado para modificar a descricao do evento
     *
     * @param strDescricao , descricao do evento
     */
    public void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    /**
     * Metodo utilizado para modificar a data de inicio do evento
     *
     * @param strDataInicio , data de inicio do evento
     */
    public void setDataInicio(String strDataInicio) {
        this.m_strDataInicio = strDataInicio;
    }

    /**
     * Metodo utilizado para modificar a data de fim do evento
     *
     * @param strDataFim , data fim do evento
     */
    public void setDataFim(String strDataFim) {
        this.m_strDataFim = strDataFim;
    }

    /**
     * Metodo utilizado para modificar a lista de organizadores evento
     *
     * @param m_listaOrganizadores, lista de organizadores do evento
     */
    public void setListaOrganizadores(List<Organizador> m_listaOrganizadores) {
        this.m_listaOrganizadores = m_listaOrganizadores;
    }

    /**
     * Metodo utilizado para modificar a lista de sessao tematica do evento
     *
     * @param m_listaSessaoTematica, lista de sessao temática do evento
     */
    public void setListaSessaoTematica(List<SessaoTematica> m_listaSessaoTematica) {
        this.m_listaSessaoTematica = m_listaSessaoTematica;
    }

    /**
     * Metodo utilizado para modificar a lista de organizadores evento
     *
     * @param m_listaSubmissoes, lista de submissoes do evento
     */
    public void setListaSubmissoes(List<Submissao> m_listaSubmissoes) {
        this.m_listaSubmissoes = m_listaSubmissoes;
    }

    /**
     * Metodo utilizado para modificar a CP do evento
     *
     * @param cp , cp do evento
     */
    public void setCP(CP cp) {
        m_cp = cp;
    }

    /**
     * Metodo utilizado para modificar o local do evento
     *
     * @param m_local, local do evento
     */
    public void setLocal(Local m_local) {
        this.m_local = m_local;
    }

    /**
     * Metodo utilizado para consultar o titulo do evento
     *
     * @return titulo do evento
     */
    public String getTitulo() {
        return m_strTitulo;
    }

    /**
     * Metodo utilizado para consultar a descrição do evento
     *
     * @return descrição do evento
     */
    public String getDescricao() {
        return m_strDescricao;
    }

    /**
     * Metodo utilizado para consultar a data de inicio do evento
     *
     * @return data de inicio do evento
     */
    public String getDataInicio() {
        return m_strDataInicio;
    }

    /**
     * Metodo utilizado para consultar a data de fim do evento
     *
     * @return data de fim do evento
     */
    public String getDataFim() {
        return m_strDataFim;
    }

    /**
     * Metodo utilizado para consultar o local do evento
     *
     * @return local do evento
     */
    public Local getLocal() {
        return m_local;
    }

    /**
     * Metodo utilizado para consultar a lista de organizadores do evento
     *
     * @return lista de organizadores do evento
     */
    public List<Organizador> getListaOrganizadores() {
        List<Organizador> lOrg = new ArrayList<>();

        for (ListIterator<Organizador> it = m_listaOrganizadores.listIterator(); it.hasNext();) {
            lOrg.add(it.next());
        }

        return lOrg;
    }

    /**
     * Metodo utilizado para consultar a lista de sessoes tematicas do evento
     *
     * @return lista de sessoes tematicas do evento
     */
    public List<SessaoTematica> getListaSessaoTematica() {
        return m_listaSessaoTematica;
    }

    /**
     * Metodo utilizado para consultar a lista de submissoes do evento
     *
     * @return lista de submissoes do evento
     */
    public List<Submissao> getListaSubmissoes() {
        return m_listaSubmissoes;
    }

    /**
     * Metodo utilizado para consultar a CP do evento
     *
     * @return a cp do evento
     */
    public CP getCp() {
        return m_cp;
    }

    /**
     * Metodo para adicionar um Organizador com os dados dele ( identificaçao e
     * o Utilizador que é organizador)
     *
     * @param u , utilizador do evento
     * @param strId, identificação do evento
     * @return addOrganizador , adicionar o Organizador do evento
     */
    public boolean addOrganizador(String strId, Utilizador u) {
        Organizador o = new Organizador(strId, u);

        return addOrganizador(o);
    }

    /**
     *
     * Metodo para adicionar um Organizador do evento
     *
     * @param o, Organizador do evento
     * @return m_listaOrganizadores.add , a lista de organizadores vai se
     * adicionar um organizador
     */
    private boolean addOrganizador(Organizador o) {
        return m_listaOrganizadores.add(o);
    }

    /**
     *
     * Metodo que retorna uma String com toda a informação de um Evento
     *
     * @return titulo, data inicio, data fim do evento
     */
    @Override
    public String toString() {
        return m_strTitulo + ", de " + m_strDataInicio + " a "
                + m_strDataFim;
    }

    /**
     *
     * Metodo para inicializar uma nova Submissão
     *
     * @return submissão
     */
    @Override
    public Submissao novaSubmissao() {
        return new Submissao();
    }

    /**
     * Metodo para adicionar uma submissao do evento
     *
     * @param s, submissao do evento
     *  Se a submissão for valida, adiciona à
     * lista de submissoes uma submissao
     */
    @Override
    public void addSubmissão(Submissao s) {
        if (s.valida()) {
            m_listaSubmissoes.add(s);
        }
    }

    /**
     *
     * Metodo para adicionar uma Sessao Temática com os dados dela ( codigo e a
     * descrição)
     *
     * @param codigo , codigo da sessao temática
     * @param descrisao , descriçaõ da sessao temática
     * @return addSessaoTematica, adicionar a Sessao Tematica do evento
     */
    public boolean addSessaoTematica(String codigo, String descrisao) {
        SessaoTematica st = new SessaoTematica(codigo, descrisao);

        return addSessaoTematica(st);
    }

    /**
     * Metodo para adicionar uma sessão temática do evento
     *
     * @param st, sessão tematica do evento
     * @return m_listaSessaoTematica.add ,adiciona à lista de sessoes tematicas
     * uma sessão
     */
    private boolean addSessaoTematica(SessaoTematica st) {
        return m_listaSessaoTematica.add(st);
    }
}
