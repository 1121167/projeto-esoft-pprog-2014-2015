package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;

public class SessaoTematica implements Submissivel {

    // Variaveis de instancia
    private String codigo;
    private String descricao;
    // Variaveis de classe
    private List<Proponente> m_listaProponentes;
    private List<Submissao> m_listaSubmissoes;
    private CP m_cp;
    
    /**
     *
     * Método construtor utilizado com informação acerca de um Autor.
     * @param codigo , codigo da sessão tematica
     * @param descricao , descricao da sessao tematica
     */
    public SessaoTematica(String codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
        m_listaProponentes = new ArrayList<>();
        m_listaSubmissoes = new ArrayList<>();
    }
    /**
     * Metodo construtor sem informação da sessao tematica
     */
    public SessaoTematica() {
        m_listaProponentes = new ArrayList<>();
        m_listaSubmissoes = new ArrayList<>();
    }
   /**
     * Metodo para inicializar uma nova CP da sessao tematica
     *
     * @return m_cp , novaCP da sessao tematica
     */
    public CP novaCP() {
        return m_cp = new CP();
    }

     /**
     * Metodo utilizado para modificar o codigo da sessao tematica
     *
     * @param codigo, codigo da sessao tematica
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
 /**
     * Metodo utilizado para modificar a descriçao da sessão tematica 
     *
     * @param descricao , descricao da sessao tematica
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
 /**
     * Metodo utilizado para modificar a lista de proponentes da sessao tematica
     *
     * @param m_listaProponentes, lista de proponente da sessao tematica
     */
    public void setListaProponentes(List<Proponente> m_listaProponentes) {
        this.m_listaProponentes = m_listaProponentes;
    }
 /**
     * Metodo utilizado para modificar a lista de submissoes da sessão temática
     *
     * @param m_listaSubmissoes, lista de submissoes da sessao tematica
     */
    public void setListaSubmissoes(List<Submissao> m_listaSubmissoes) {
        this.m_listaSubmissoes = m_listaSubmissoes;
    }
 /**
     * Metodo utilizado para modificar a CP da sessao tematica
     *
     * @param m_cp , cp da sessao tematica
     */
    public void setCp(CP m_cp) {
        this.m_cp = m_cp;
    }
 /**
     * Metodo utilizado para consultar o codigo da sessao tematica 
     *
     * @return codigo , codigo da sessao tematica
     */
    public String getCodigo() {
        return codigo;
    }
 /**
     * Metodo utilizado para consultara descricao da sessao tematica
     *
     * @return descricao , descricao da sessao tematica
     */
    public String getDescricao() {
        return descricao;
    }
 /**
     * Metodo utilizado para consultar a lista de proponentes da sessao tematica 
     *
     * @return m_listaProponentes, lista de proponentes da sessao tematica
     */
    public List<Proponente> getListaProponentes() {
        return m_listaProponentes;
    }
 /**
     * Metodo utilizado para consultar a lista de submissoes da sessao tematica
     *
     * @return m_listaSubmissoes, lista de submissoes da sessao tematica
     */
    public List<Submissao> getListaSubmissoes() {
        return m_listaSubmissoes;
    }
 /**
     * Metodo utilizado para consultar a cp da sessao tematica 
     *
     * @return cp, cp da sessao tematica
     */
    public CP getCp() {
        return m_cp;
    }

      /**
     *
     * Metodo para adicionar um Proponente à sessao tematica
     *
     * @param p, Proponente da sessao
     * @return m_listaProponentes.add , a lista de proponentes vai se
     * adicionar um proponente 
     */
    private boolean addProponente(Proponente p) {
        return m_listaProponentes.add(p);
    }
 /**
     * Metodo que retorna uma String com toda a informação de uma sessao tematica
     * return codigo , codigo da sessão
     */
    @Override
    public String toString() {
        return codigo;
    }
   /**
     * Metodo para inicializar uma nova submissao da sessao tematica
     *
     * @return submissao , submissao da sessao
     */
    @Override
    public Submissao novaSubmissao() {
        return new Submissao();
    }
 
      /**
     *
     * Metodo para adicionar uma submissao à sessao tematica
     *
     * @param s, Submissao da sessao
     * Caso a submissão seja valida, à lista de submissoes vai se
     * adicionar um submissão
     */
    public void addSubmissão(Submissao s) {
        if (s.valida()) {
            m_listaSubmissoes.add(s);
        }
    }

}
