package eventoscientificos.model;

public class Organizador {

    // Variaveis de Classe
    private final String m_strNome;
    private Utilizador m_utilizador;

    /**
     * Método construtor utilizado com informação acerca de um organizador.
     *
     * @param u, utilizador que é organizador
     * @param strId , identificação do organizador
     */
    public Organizador(String strId, Utilizador u) {
        m_strNome = u.getNome();
        this.setUtilizador(u);
    }

    /**
     * Metodo utilizado para modificar o atributo utilizador do Organizador
     *
     * @param utilizador, utilizador que é organizador
     */
    private void setUtilizador(Utilizador u) {
        m_utilizador = u;
    }

    /**
     * Metodo utilizado para consultar o nome do organizador
     *
     * @return nome do organizador
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     * Metodo utilizado para consultar o utilizador que é organizador
     *
     * @return utilizador que é organizador
     */
    public Utilizador getUtilizador() {
        return m_utilizador;
    }

    /**
     *
     * Metodo que retorna uma String com toda a informação de um Organizador
     *
     * @return dados do utilizador que é organizador
     */
    @Override
    public String toString() {
        return m_utilizador.toString();
    }
}
