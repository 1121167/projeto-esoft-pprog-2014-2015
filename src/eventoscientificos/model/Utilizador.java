package eventoscientificos.model;

import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilizador {

    //Variaveis de instancia
    private String nome;
    private String email;
    private String username;
    private String password;
    // Variavel de classe
    private static List<Utilizador> m_listaUtilizadores = new ArrayList<Utilizador>();
    
    /**
     * Método construtor utilizado com informação acerca de um utilizador, so com o nome, email, username e  password.
     * @param nome, nome do Utilizador
     * @param email , email do Utilizador
     * @param username ,username do Utilizador
     * @param password , password do Utilizador
     */
    public Utilizador(String nome, String email, String username, String password) {
        this.nome = nome;
        this.email = email;
        this.username = username;
        this.password = password;
    }
    
    public Utilizador(){
        
    }
/**
 * 
 * @return 
 */
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static List<Utilizador> getListaUtilizadores() {
        return m_listaUtilizadores;
    }

    public void addUtilizador() {
        if (valida()) {
            m_listaUtilizadores.add(this);
        }
    }

    @Override
    public String toString() {
        return nome + "; " + email + "; " + username;
    }

    public boolean valida() {
        if ((this.nome).equalsIgnoreCase(null)) {
            return false;
        } else if ((this.username).equalsIgnoreCase(null)) {
            return false;
        } else if ((this.password).length() < 6) {
            return false;
        }
        for (int i = 0; i < m_listaUtilizadores.size(); i++) {
            if ((this.username).equalsIgnoreCase(m_listaUtilizadores.get(i).getUsername())) {
                return false;
            }
            if ((this.email).equalsIgnoreCase(m_listaUtilizadores.get(i).getEmail())) {
                return false;
            }
        }
        return validaEmail();
    }

    public boolean validaEmail() {
        if (((this.email).equalsIgnoreCase(null))) {
            return false;
        }
        String emailP = "\\b(^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";
        Pattern p = Pattern.compile(emailP, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(email);
        return m.matches();
    }
}

