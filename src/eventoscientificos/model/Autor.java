package eventoscientificos.model;

import java.util.regex.*;

public class Autor {

    //Variaveis instancia
    private String nome;
    private String email;
    private String afiliacao;

    /**
     *
     * Método construtor utilizado com informação acerca de um Autor.
     *
     * @param nome , nome do autor
     * @param email , email do autor
     * @param afiliacao , afiliacao do autor
     */
    public Autor(String nome, String email, String afiliacao) {
        this.nome = nome;
        this.email = email;
        this.afiliacao = afiliacao;
    }

    /**
     * Metodo construtor sem informação do Autor
     */
    public Autor() {
    }

    /**
     * Metodo utilizado para consultar o nome do autor
     *
     * @return nome, nome do autor
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * Metodo utilizado para consultar o email do Autor
     *
     * @return email, email do Autor
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * Metodo utilizado para consultar o afilição do Autor
     *
     * @return afiliacao, afiliacao do Autor
     */
    public String getAfiliacao() {
        return afiliacao;
    }

    /**
     * Metodo utilizado para modificar o atributo nome do Autor
     *
     * @param nome, nome do Autor
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Metodo utilizado para modificar o email do Autor
     *
     * @param email, email do Autor
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * Metodo utilizado para modificar o atributo afiliação do Autor
     *
     * @param afiliacao , afiliação do Autor
     */
    public void setAfiliacao(String afiliacao) {
        this.afiliacao = afiliacao;
    }

    /**
     * Metodo valida os dados do autor( nome e afiliação)
     *
     * @return false se os dados do autor não forem validos senao return
     * validaEmail do autor
     */
    public boolean valida() {
        if ((this.nome).equalsIgnoreCase(null)) {
            return false;
        } else if ((this.afiliacao).equalsIgnoreCase(null)) {
            return false;
        }
        return validaEmail();
    }

    /**
     * Metodo valida o email e o dado do email( email)
     *
     * @return false se email não for valido senao return m.matches do email
     */
    public boolean validaEmail() {
        if (((this.email).equalsIgnoreCase(null))) {
            return false;
        }
        String emailP = "\\b(^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";
        Pattern p = Pattern.compile(emailP, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    /**
     *
     * Metodo que retorna uma String com toda a informação de um Autor
     *
     * @return nome do Autor , email do autor e afiliação do autor
     */
    @Override
    public String toString() {
        return nome + "; " + email + "; " + afiliacao;
    }
}
