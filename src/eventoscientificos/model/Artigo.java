package eventoscientificos.model;

import java.io.*;
import java.util.*;

public class Artigo {

    // Variaveis de Instancia
    private String titulo;
    private String resumo;
    private File ficheiro;

    // Variaveis de classe
    private Autor autorCorrespondente;
    private List<Autor> listaAutores;

    /**
     * Metodo construtor sem informação do Artigo
     */
    public Artigo() {
        this.listaAutores = new ArrayList<>();
    }

    /**
     * Metodo utilizado para consultar o titulo do artigo
     *
     * @return titulo, titulo do artigo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Metodo utilizado para modificar o atributo titulo do Artigo
     *
     * @param titulo, titulo do Artigo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Metodo utilizado para consultar o resumo do artigo
     *
     * @return resumo, resumo do artigo
     */
    public String getResumo() {
        return resumo;
    }

    /**
     * Metodo utilizado para modificar o atributo resumo do Artigo
     *
     * @param resumo, resumo do Artigo
     */
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    /**
     * Metodo utilizado para consultar o ficheiro do Artigo
     *
     * @return ficheiro, ficheiro do artigo
     */
    public File getFicheiro() {
        return ficheiro;
    }

    /**
     * Metodo utilizado para modificar o atributo ficheiro do Artigo
     *
     * @param ficheiro, ficheiro do Artigo
     */
    public void setFicheiro(File ficheiro) {
        this.ficheiro = ficheiro;
    }

    /**
     * Metodo utilizado para consultar a lista de autores do artigo
     *
     * @return listaAutores, listaAutores do artigo
     */
    public List<Autor> getListaAutores() {
        return listaAutores;
    }

    /**
     * Metodo utilizado para modificar o atributo da lista de autores do Artigo
     *
     * @param listaAutores, listaAutores do Artigo
     */
    public void setListaAutores(List<Autor> listaAutores) {
        this.listaAutores = listaAutores;
    }

    /**
     * Metodo para inicializar um novo Autor
     *
     * @param nome, nome do autor
     * @param afiliacao , afiliaçao do autor
     * @param email , email do autor
     * @return a1 , autor
     */
    public Autor novoAutor(String nome, String afiliacao, String email) {
        Autor a1 = new Autor();
        a1.setNome(nome);
        a1.setAfiliacao(afiliacao);
        a1.setEmail(email);
        return a1;
    }

    /**
     * Metodo para adicionar um Autor
     *
     * @param aut , autor do Artigo
     * @return true caso o autor seja valido senao return false
     */
    public boolean addAutor(Autor aut) {
        if (aut.valida()) {
            listaAutores.add(aut);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metodo utilizado para modificar o atributo autor correspondente do Artigo
     *
     * @param aut, Autor correspondente do Artigo
     */
    public void setAutorCorrespondente(Autor aut) {
        this.autorCorrespondente = aut;
    }

    /**
     *  Metodo valida os dados do artigo ( titulo,resumo e ficheiro)
     * @return false se os dados do artigo não forem validos senao return true
     */
    public boolean valida() {
        if (this.titulo.equalsIgnoreCase(null)) {
            return false;
        } else if (this.resumo.equalsIgnoreCase(null)) {
            return false;
        } else if (this.ficheiro.equals(null)) {
            return false;
        }
        return true;
    }

    /**
     *
     * Metodo que retorna uma String com toda a informação de um Artigo
     *
     * @return titulo,resumo,ficheiro, autor correspondente e lista de autores
     * do artigo
     */
    @Override
    public String toString() {
        return titulo + "; " + resumo + ", ficheiro=" + ficheiro
                + ", autorCorrespondente=" + autorCorrespondente
                + ", listaAutores=" + listaAutores;
    }
}
