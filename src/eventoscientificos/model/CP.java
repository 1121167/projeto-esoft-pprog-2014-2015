package eventoscientificos.model;

import java.util.*;

public class CP {
// Variaveis de Classe

    private List<Revisor> m_listaRevisores;

    /**
     * Método construtor utilizado sem informação acerca de uma cp.
     */
    public CP() {
        m_listaRevisores = new ArrayList<>();
    }

    /**
     *
     * Metodo utilizado para consultar a lista de revisores desta classe
     *
     * @return m_listaRevisores, lista de revisores
     */
    public List<Revisor> getM_listaRevisores() {
        return m_listaRevisores;
    }

    /**
     * Metodo para adicionar o Revisor à CP
     *
     * @param strId , identificaçao do revisor
     * @param u , utilizador que é revisor
     * @return addRevisor , adicionar o Revisor
     */

    public boolean addRevisor(String strId, Utilizador u) {
        Revisor r = new Revisor(strId, u);

        return addRevisor(r);
    }

    /**
     * Metodo utilizado para adicionar Revisor
     *
     * @param r , revisor
     * @return m_listaRevisores.add(r) , lista de revisores
     */
    private boolean addRevisor(Revisor r) {
        return m_listaRevisores.add(r);
    }
}
