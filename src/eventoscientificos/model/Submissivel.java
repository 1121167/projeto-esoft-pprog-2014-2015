package eventoscientificos.model;

public interface Submissivel {

    // Inicializa a submissão
    public Submissao novaSubmissao();

    // Valida e adiciona submissao à lista de submissoes
    public void addSubmissão(Submissao m_submissao);
}
