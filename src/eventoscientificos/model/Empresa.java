package eventoscientificos.model;

import java.util.*;

public class Empresa {
//Variaveis de Classe

    private List<Utilizador> m_listaUtilizadores;
    private List<Evento> m_listaEventos;

    /**
     * Metodo construtor sem informação da Empresa
     */
    public Empresa() {
        m_listaUtilizadores = new ArrayList<>();
        m_listaEventos = new ArrayList<>();
    }

    /**
     * Metodo utilizado para consultar o a lista de utilizadores da empresa
     *
     * @return a lista de utilizadores
     */
    public List<Utilizador> getListaUtilizadores() {
        return m_listaUtilizadores;
    }

    /**
     * Metodo utilizado para modificar o atributo lista de utilizadores da
     * Empresa
     *
     * @param m_listaUtilizadores, lista de utilizadores da Empresa
     */
    public void setListaUtilizadores(List<Utilizador> m_listaUtilizadores) {
        this.m_listaUtilizadores = m_listaUtilizadores;
    }

    /**
     * Metodo utilizado para consultar o a lista de eventos da empresa
     *
     * @return a lista de eventos
     */
    public List<Evento> getListaEventos() {
        return m_listaEventos;
    }

    /**
     * Metodo utilizado para modificar o atributo lista de eventos da Empresa
     *
     * @param m_listaEventos, lista de eventos da Empresa
     */
    public void setListaEventos(List<Evento> m_listaEventos) {
        this.m_listaEventos = m_listaEventos;
    }

    /**
     * Metodo para inicializar um novo Utilizador na Empresa
     *
     * @return Utilizador, novo Utilizador na Empresa
     */
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    /**
     * Metodo para registar um Utilizador, apos registar adiciona o Utilizador
     *
     * @param u , utilizador
     * @return addUtilizador , adiciona o Utilizador
     */
    public boolean registaUtilizador(Utilizador u) {
        return addUtilizador(u);
    }
/**
 *  Metodo para validar o utilizador na empresa
 * @param u , Utilizador da empresa
 * @return u , utilizador encontra-se na empresa
 */
    private boolean validaUtilizador(Utilizador u) {
        return u.valida();
    }

    /**
     * Metodo para inicializar um novo evento na Empresa
     *
     * @return evento, novo evento na Empresa
     */
    public Evento novoEvento() {
        return new Evento();
    }

    /**
     *
     * Metodo para registar um Evento, após registar adiciona o Evento
     *
     * @param e , evento
     * @return addEvento , adicionar o Evento
     */
    public boolean registaEvento(Evento e) {
        return addEvento(e);
    }

    /**
     *
     * Metodo para adicionar o evento
     *
     * @param u , utilizador
     * @return m_listaUtilizadores.add(u) , da lista de utilizadores vai
     * adicionar um utilizador à empresa
     */
    private boolean addUtilizador(Utilizador u) {
        return m_listaUtilizadores.add(u);
    }

    /**
     * Metodo para adicionar o evento
     *
     * @param e , evento
     * @return m_listaEventos.add(e) , da lista de eventos vai adicionar um
     * evento à empresa
     */
    private boolean addEvento(Evento e) {
        return m_listaEventos.add(e);
    }

    /**
     * Metodo utilizado para consultar o a lista submissiveis da empresa
     *
     * @return a lista de submissiveis
     */
    public List<Submissivel> getListaSubmissiveis() {
        List<Submissivel> listaSub = new ArrayList<Submissivel>();
        for (int i = 0; i < m_listaEventos.size(); i++) {
            listaSub.add(m_listaEventos.get(i));
        }
        return listaSub;
    }
}
