package eventoscientificos.model;

public class Revisor {
//Variaveis de classe
    private final String m_strNome;
    private Utilizador m_utilizador;

    /**
     * Método construtor utilizado com informação acerca de um Revisor.
     *
     * @param strId, identificação do Revisor
     * @param u , utilizador que é revisor
     */
    public Revisor(String strId, Utilizador u) {
        m_strNome = u.getNome();
        this.setUtilizador(u);
    }

    /**
     * Metodo utilizado para modificar o atributo utilizador do Revisor
     *
     * @param u utilizador, da Classe revisor
     */
    private void setUtilizador(Utilizador u) {
        this.m_utilizador = u;
    }

    /**
     * Metodo utilizado para consultar o nome do revisor
     *
     * @return m_strNome, Nome do Revisor
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     * Metodo utilizado para consultar o utilizador
     *
     * @return m_utilizador, Utilizador do revisor
     */
    public Utilizador getUtilizador() {
        return m_utilizador;
    }

    /**
     * Metodo que retorna uma String com toda a informação de um Revisor
     */
    @Override
    public String toString() {
        return m_utilizador.toString();
    }
}
