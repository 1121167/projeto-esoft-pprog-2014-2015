package eventoscientificos.model;

public class Proponente {

    //Variaveis de Classe
    private final String m_strNome;
    private Utilizador m_utilizador;

    /**
     *
     * Método construtor utilizado com informação acerca do Proponente, com a
     * identificação e utilizador.
     *
     * @param strId, identificação do Proponente
     * @param u, utilizador Proponente
     */
    public Proponente(String strId, Utilizador u) {
        m_strNome = u.getNome();
        this.setUtilizador(u);
    }
    
     /**
     * Método construtor utilizado sem informação acerca do Proponente.
     */
    public Proponente() {
        this.m_strNome = "sem nome";
    }

   /**
     * Metodo utilizado para modificar o atributo utilizador do Proponente
     *
     * @param u , Utilizador Proponente
     */
    private void setUtilizador(Utilizador u) {
        m_utilizador = u;
    }
 /**
     * Metodo utilizado para consultar o nome do Proponente
     *
     * @return m_strNome, Nome do Proponente
     */
    public String getNome() {
        return m_strNome;
    }
    /**
     * Metodo utilizado para consultar o Utilizador da classe "Proponente"
     *
     * @return m_utilizador, Utilizador Proponente
     */
    public Utilizador getUtilizador() {
        return m_utilizador;
    }
    /**
     *
     * Metodo que retorna uma String com toda a informação de um Proponente
     * @return m_utilizador, dados do utilizador
     */
    @Override
    public String toString() {
        return m_utilizador.toString();
    }
}
