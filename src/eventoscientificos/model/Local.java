package eventoscientificos.model;

public class Local {
    
    //Variaveis de instância
    private String m_strLocal;
      
    /**
     * Método construtor utilizado com informação acerca de um local, so com o local.
     * @param local , local de Submissão
     */ 
    public Local ( String local){
        this.m_strLocal = m_strLocal;
    }
    
    /** 
     * Método construtor utilizado sem informação acerca do local.
     */
    public Local() {
    }
    
     /**
     * Metodo utilizado para modificar o atributo local do Local
     * @param strLocal do Local
     */
    public void setLocal(String strLocal) {
        m_strLocal = strLocal;
    }

      /**
     * Metodo utilizado para consultar o local desta classe
     *
     * @return m_strLocal, local da submissão
     */
    public String getLocal() {
        return m_strLocal;
    }

    /**
     * Metodo que retorna uma String com toda a informação de um Local
     */
    @Override
    public String toString() {
        return m_strLocal;

    }
}
