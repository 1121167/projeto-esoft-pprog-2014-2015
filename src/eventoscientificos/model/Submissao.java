package eventoscientificos.model;

public class Submissao {

    //Variaveis de instancia

    private Artigo m_artigo;

    /**
     * Metodo construtor sem informação da Submissao
     */
    public Submissao() {
    }

    /**
     * Metodo para inicializar um novo Artigo
     *
     * @return
     */
    public Artigo novoArtigo() {
        return null;
    }

    /**
     * Metodo utilizado para consultar o artigo da submissao
     *
     * @return artigo da submissao
     */
    public Artigo getArtigo() {
        return m_artigo;
    }

    /**
     * Metodo utilizado para modificar o atributo artigo da Submissao
     *
     * @param artigo , artigo da Submissao
     */
    public void setArtigo(Artigo artigo) {
        this.m_artigo = artigo;
    }

    /**
     *
     * Metodo que retorna uma String com toda a informação de uma Submissão
     *
     * @return artigo Submetido
     */
    public String toString() {
        return m_artigo.toString();
    }

    /**
     * Metodo para validar o artigo da Submissao
     *
     * @return m_artigo, artigo encontra-se na submissao
     */
    public boolean valida() {
        return this.m_artigo.valida();
    }
}
